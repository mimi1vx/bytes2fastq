
class FakeOpen:
    def __init__(self, cls, *args, **kwds):
        self.cls = cls

    def __call__(self, *args, **kwargs):
        return self.cls


class FakeFile:
    def __init__(self):
        self.result = []

    def write(self, data, *args, **kwds):
        self.result += data

    def __enter__(self):
        return self

    def __exit__(self, t, v, tb):
        print(str(self))

    def __str__(self):
        return "".join(self.result)


class FakePath:
    open = FakeOpen(FakeFile())

    def __init__(self):
        self.file = FakeFile()

from pathlib import Path

from .fakefile import FakePath

from bytes2fastq.btof import BytesToFASTQ


root = Path(__file__).parent / "testdata"

data = root / "myfile"
output16 = root / "output16"


def test_init_baseclass():
    BytesToFASTQ(Path("test"), 10)

def test_full_16(capsys):
    instance = BytesToFASTQ(data, 16)
    instance.out = FakePath()
    instance()
    captured = capsys.readouterr()
    assert output16.read_text() == captured.out[:-1]

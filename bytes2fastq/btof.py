from itertools import islice
from logging import getLogger
from pathlib import Path
from string import Template

from .reader import read_chunked_binary

logger = getLogger("bytes2fastq.btof")


class BytesToFASTQ:
    __slots__ = ["inputfile", "lvalue", "out"]

    g_table = {0b00: "A", 0b01: "C", 0b10: "G", 0b11: "T"}
    template = Template("@READ_${index}\n${dnadata}\n+READ_${index}\n${quality}\n")

    def __init__(self, inputfile: Path, lvalue: int):
        self.inputfile = inputfile
        self.lvalue = lvalue
        # for testability
        self.out = Path(f"./output{lvalue}")

    def __call__(self):
        logger.debug(f"open for write ./output{self.lvalue} file")
        try:
            with self.out.open(mode="w") as out:
                iterable = self.t_generator(read_chunked_binary(self.inputfile))
                counter = 1
                while True:
                    dnatuple = self.take(self.lvalue, iterable)
                    if dnatuple == ("", ""):
                        break
                    text = self.prepare_string(counter, dnatuple)
                    logger.info(f"Writing {counter} record")
                    counter += 1
                    logger.debug(f"writting {text}")
                    out.write(text)
        except (IOError, PermissionError) as e:
            logger.error(e)
            return 1
        return 0

    @classmethod
    def t_generator(cls, data):
        """generator expression returning tuples - (letter, quality)"""
        return ((cls.g_table[i >> 6], chr((i & 0b00111111) + 33)) for i in data)

    @staticmethod
    def take(n, iterable):
        data = islice(iterable, n)
        dna = ""
        quality = ""
        for i, j in data:
            dna += i
            quality += j
        return dna, quality

    @classmethod
    def prepare_string(cls, index, dnatuple):
        logger.debug(f"String {index} are {dnatuple}")
        dnadata, quality = dnatuple
        return cls.template.substitute(index=index, dnadata=dnadata, quality=quality)

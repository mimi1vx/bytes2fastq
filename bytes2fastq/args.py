import argparse
from pathlib import Path


def get_argparser():
    parser = argparse.ArgumentParser(description="Binary format to FASTQ converter")
    group = parser.add_mutually_exclusive_group()
    group.add_argument("--debug", action="store_true", help="Run in debug mode")
    group.add_argument("--verbose", action="store_true", help="Run in verbose mode")

    parser.add_argument(
        "inputfile", type=Path, help="Input file with binary genomics data",
    )
    parser.add_argument(
        "lval", metavar="L-value", type=int, help="Length of data string [1...]"
    )

    return parser

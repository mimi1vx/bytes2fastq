from pathlib import Path
from logging import getLogger

# sample size, need to adjust for optimal performace
logger = getLogger("bytes2fastq.reader")
CHUNK_SIZE = 512


def read_chunked_binary(path, chunksize=CHUNK_SIZE):
    logger.debug(f"Opening {path} with chunksize {chunksize}")
    with path.open(mode="rb") as f:
        while chunk := f.read(chunksize):
            yield from chunk

import logging
import sys

from .args import get_argparser
from .btof import BytesToFASTQ


def main():
    logger = logging.getLogger("bytes2fastq")
    handler = logging.StreamHandler()
    formatter = logging.Formatter("%(levelname)-2s: %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    parser = get_argparser()

    args = parser.parse_args(sys.argv[1:])

    sys.exit(run_b2f(logger, args))


def run_b2f(logger, args):
    if args.verbose:
        logger.setLevel("INFO")
    elif args.debug:
        logger.setLevel("DEBUG")
    else:
        # no warnings so no output by default
        logger.setLevel("WARNING")

    if not args.inputfile.is_file():
        logger.error("inputfile must be binary file")
        return 1

    if args.lval < 1:
        logger.error("L-value must be 1 or higher")
        return 2

    runner = BytesToFASTQ(args.inputfile, args.lval)
    return runner()

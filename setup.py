#!/usr/bin/python3

from setuptools import setup, find_packages


setup(
    name="bytes2fastq",
    license="GPL-3-or-later",
    version="0.0.1",
    description="Small util converting binary data to FASTQ format",
    url="https://gitlab.com/mimi1vx/bytes2fastq",
    author="Ondřej Súkup",
    author_email="mimi.vx+gitlab@gmail.com",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "License :: OSI Approved :: GPL-3-or-later",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3 :: Only",
    ],
    packages=find_packages(exclude=["*.tests", "*.tests.*", "tests.*", "tests"]),
    entry_points={"console_scripts": ["bytest2fastq = bytes2fastq.main:main"]},
    python_requires=">=3.8",
    tests_require="pytest",
)
